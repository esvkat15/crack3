#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(const void* key, const void* guess) {
    
    // Compare the two hashes
    return strncmp((*(char**) key), *((char**) guess), HASH_LEN - 1);
    
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename) {
    
    struct stat filesize;
    if(stat(filename, &filesize) == -1) {
        
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
        
    }
    int size = filesize.st_size;
    char* buffer = malloc((size + 1) * sizeof(char));
    FILE* fp = fopen(filename, "r");
    if(!fp) {
        
        fprintf(stderr, "Can't open %s\n", filename);
        exit(1);
        
    }
    fread(buffer, 1, size, fp);
    fclose(fp);
    buffer[size] = '\0';
    int i = 1;
    char** spointers = malloc(sizeof(char*));
    *spointers = buffer;
    do if(*buffer == '\n') {
        
        *buffer = '\0';
        spointers = realloc(spointers, (i + 1) * sizeof(char*));
        spointers[i++] = buffer + 1;
        
    } while(*(++buffer));
    spointers = realloc(spointers, (i + 1) * sizeof(char*));
    spointers[i] = NULL;
    return spointers;
    
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename) {
    
    FILE* fp = fopen(filename, "r");
    if(!fp) {
        
        fprintf(stderr, "Can't open %s\n", filename);
        exit(1);
        
    }
    char** spointers = malloc(sizeof(char*));
    int sptrs = 1;
    char temp[PASS_LEN];
    while(fgets(temp, PASS_LEN, fp)) {
        
        spointers[sptrs - 1] = malloc((PASS_LEN + HASH_LEN + 3) * sizeof(char));
        spointers = realloc(spointers, ++sptrs * sizeof(char*));
        temp[strlen(temp)-1] = '\0';
        char* guesh = md5(temp, strlen(temp));
        sprintf(spointers[sptrs - 2], "%s : %s", guesh, temp);
        free(guesh);
        
    }
    fclose(fp);
    spointers[sptrs - 1] = NULL;
    return spointers;
    
}


int main(int argc, char *argv[ ]) {
    
    if (argc != 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    int length = 0;
    while(dict[++length]);
    qsort(dict, length, sizeof(char*), tryguess);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int s = 0;
    while(hashes[++s]) printf("%s\n", *(char**) bsearch(&hashes[s - 1], dict, length, sizeof(char*), tryguess));
    
    free(*hashes);
    free(hashes);
    length++;
    while(length) free(dict[--length]);
    free(dict);
    
}